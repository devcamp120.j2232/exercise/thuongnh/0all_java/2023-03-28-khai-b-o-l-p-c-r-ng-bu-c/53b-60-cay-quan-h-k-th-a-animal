public class Animal {
    protected String  name;

    
    // Animal (động vật )
    public Animal(String name){
        this.name = name;
    }

    @Override
    public String toString(){
        return "Animal [ name=" + name + "]";
    }

    
}
